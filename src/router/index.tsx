import App from "App7";
import Home from "pages/Home";
import List from "pages/List";
import Detail from "pages/Detail";
import About from "pages/About";
import Login from "pages/Login";
import NoMatch from "pages/NoMatch";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

const MyRouter = () => (
  <Router>
    <Routes>
      <Route path="/" element={<App />}>
        <Route index element={<Home />}></Route>
        <Route path="/list/" element={<List />}></Route>
        <Route path="/list/:id" element={<List />}></Route>
        <Route path="/detail" element={<Detail />}></Route>
        <Route path="/about" element={<About />}></Route>
      </Route>
      <Route path="*" element={<NoMatch />}></Route>
      <Route path="/login" element={<Login />}></Route>
    </Routes>
  </Router>
);

export default MyRouter;
