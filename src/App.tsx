import React, { useState } from "react";

const App: React.FC = () => {
  const [num, setNum] = useState(0);
  const addNumFn = () => {
    setNum(num + 1);
  };
  return (
    <div>
      <h2>你好世界</h2>
      <h2>数字为: {num}</h2>
      <button onClick={addNumFn}>累加</button>
    </div>
  );
};

export default App;
