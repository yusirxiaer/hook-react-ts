import React from "react";
import { Outlet, Link } from "react-router-dom";

function App() {
  return (
    <div>
      <ul>
        <li>
          <Link to={"/list"}>列表页(不带id)</Link>
        </li>
        <li>
          <Link to={"/list/456"}>列表页(带id)</Link>
        </li>
        <li>
          <Link to={"/detail?id=123"}>详情页</Link>
        </li>
        <li>
          <Link to={"/about"}>关于我们</Link>
        </li>
      </ul>
      <Outlet />
    </div>
  );
}

export default App;
