import React, { useEffect, useState } from "react";

export default function App3() {
  const [num1, setNum1] = useState(0);
  const [num2, setNum2] = useState(0);

  // useEffect（callback，array）
  useEffect(() => {
    //建议在这个callback中做请求
    console.log("componentDidMount");
  }, []); //空数组意味着不检测任何数据的更新

  useEffect(() => {
    console.log("componentDidUpdate");
  }, [num1, num2]); // 数组中如果包含了页面中所有会更新的数据，那这个数组也可以不写

  useEffect(() => {
    //组件卸载时会触发callback中返回的函数
    return () => {
      //建议在这个位置清除一些残余的数据
      console.log("componentDidMount");
    };
  }, []);

  return (
    <div>
      <h2>{num1}</h2>
      <button onClick={() => setNum1(num1 + 1)}>累加num1</button>
      <hr />
      <h2>{num2}</h2>
      <button onClick={() => setNum2(num2 + 1)}>累加num2</button>
    </div>
  );
}

/**
    函数式组件和类组件的不同之处：
    没有this      不用this
    没有state     hook  - usestate
    没有生命周期   hook-useEffect
    用useEffect模拟生命周期
    componentDidMount 
    componentDidUpdate
    componentWillUnmount
*/
