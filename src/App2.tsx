import React, { useState } from "react";

interface IProps {
  num: number;
  toSetNum: (num: number) => void;
}
const Sub: React.FC<IProps> = (props: IProps) => {
  return (
    <>
      <h2>你好世界</h2>
      <h2>数字为: {props.num}</h2>
      <button onClick={() => props.toSetNum(2)}>累加</button>
    </>
  );
};
const App1: React.FC = () => {
  const [num, setNum] = useState(0);
  const toSetNum = (arg: number) => {
    setNum(num + arg);
  };
  return (
    <div>
      <Sub num={num} toSetNum={toSetNum} />
    </div>
  );
};

export default App1;
