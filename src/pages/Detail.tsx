import React from "react";
import { useNavigate, useSearchParams } from "react-router-dom";

export default function Detail() {
  const [params] = useSearchParams();
  const navigate = useNavigate();
  let myId = params.getAll("id")[0];

  const goHome = () => {
    navigate("/", {
      state: { id: 789 },
    });
  };
  return (
    <div>
      <h1>Detail - id: {myId}</h1>
      <h1>yusir</h1>
      <button onClick={goHome}>跳转首页</button>
    </div>
  );
}
