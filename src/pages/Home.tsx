import React from "react";
import { useLocation } from "react-router-dom";

interface ILocation {
  hash: string;
  key: string;
  pathname: string;
  search: string;
  state?: any;
}

export default function Home() {
  let location: ILocation = useLocation();

  return (
    <div>
      <h1>首页- id: {location?.state.id} </h1>
      <h1>yusir</h1>
    </div>
  );
}
