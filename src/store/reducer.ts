interface IDefaultstate {
  num: number;
}

// 定义了默认数据
const defaultState: IDefaultstate = {
  num: 1,
};
//action的接口
interface IAction {
  type: string;
  value?: number;
}

//导出一个函数
// eslint-disable-next-line
export default (state = defaultState, action: IAction) => {
  let newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case "increase":
      newState.num += action.value;
      break;
    default:
      break;
  }
  return newState;
};
