import React, { useState } from "react";

interface IProps {
  num: number;
  setNum: (num: number) => void;
}
const Sub: React.FC<IProps> = (props: IProps) => {
  return (
    <>
      <h2>你好世界</h2>
      <h2>数字为: {props.num}</h2>
      <button onClick={() => props.setNum(props.num + 1)}>累加</button>
    </>
  );
};
const App1: React.FC = () => {
  const [num, setNum] = useState(0);

  return (
    <div>
      <Sub num={num} setNum={setNum} />
    </div>
  );
};

export default App1;
